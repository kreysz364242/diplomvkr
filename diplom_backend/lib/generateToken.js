const jwt = require('jsonwebtoken');
const Users = require('../models/UserModel')
const RefreshTokens = require('../models/refreshTokenModel')

module.exports = {
    generateAccessJWT: (id, generateTime) => {
        return jwt.sign({
            id,
            exp: generateTime,
        }, 'secret')
    },

    generateRefreshJWT: (UserId, fingerprint, updateAt) => new Promise(async (resolve, reject) => {
        try {
            console.log(UserId, fingerprint, updateAt)
            const refreshTokens = await RefreshTokens.findAll({ where: { UserId } })
            if (refreshTokens.length >= 5)
                await RefreshTokens.destroy({ where: { UserId } })
            const token = await RefreshTokens.create({ fingerprint, token: '', updateAt })
            await RefreshTokens.update({ UserId, token: jwt.sign({ id: token.id, exp: updateAt }, 'secret') }, {
                where: {
                    id: token.id
                }
            })
            const newToken = await RefreshTokens.findOne({where: { id: token.id }})
            if(!newToken) throw new Error()
            resolve(newToken.token)
        }catch(error){
            reject(new Error())
        }

    })
}


