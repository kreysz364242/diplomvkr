

module.exports = {
    generateTimeForAccessToken: (minutes = 30)=>{
        const today = new Date();
        const expirationDate = new Date(today);
        expirationDate.setMinutes(today.getMinutes() + minutes);
        return parseInt(expirationDate.getTime() / 1000, 10)
    },

    generateTimeForRefreshToken: (days = 30)=>{
        const today = new Date();
        const expirationDate = new Date(today);
        expirationDate.setDate(today.getDate() + days);
        return parseInt(expirationDate.getTime() / 1000, 10)
    }
}