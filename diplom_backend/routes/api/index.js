const express = require('express');
const router = express.Router();


router.use('/admin', require('./admin'));
router.use('/places', require('./places'))
router.use('/comments', require('./comments'))

module.exports = router;