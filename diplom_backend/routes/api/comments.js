const router = require('express').Router();
const passport = require('passport')
const Comments = require('../../models/commentsModel')


router.post('/addComment', passport.authenticate('jwt', { session: false }), async(req, resp, next) => {
    const { body: { comment } } = req;
    const { user } = req
    if (!comment) return next(createError('comment not exist', 402))
    try {
        await Comments.create({ commentText: comment.commentText, PlaceId: comment.id, UserId: user.id })
    } catch (error) {
        return next(createError('db error', 500))
    }
})

module.exports = router