// const router = require('express').Router();
// const token = require('../../lib/decodeTokens')
// const jwt = require('jsonwebtoken');
// const RefreshTokens = require('../../models/refreshTokenModel')
// const generateTokens = require('../../lib/generateToken')
// const { generateAccessJWT } = generateTokens
// const { generateTimeForAccessToken, generateTimeForRefreshToken } = require('../../lib/generateTimeToToken')
// const createError = require('../../lib/createError')


// router.post('/', token.required, async (req, resp, next) => {
//     const { payload: { id } } = req;
//     const { body: { fingerprint } } = req;
//     console.log(id, fingerprint)
//     try {
//         const refreshToken = await RefreshTokens.findOne({ where: { id: id } })
//         if (!refreshToken) return next(createError('Token is invalid', 402))
//         if (refreshToken.fingerprint !== fingerprint) {
//             await RefreshTokens.destroy({ where: { id: id } })
//             return next(createError('Token is invalid', 402))
//         }
//         const time = generateTimeForRefreshToken()
//         refreshToken.token = jwt.sign({ id: refreshToken.id, exp: time }, 'secret')
//         refreshToken.updateAt = time
//         const updateToken = await refreshToken.save()
//         return resp.status(200).json({
//             accessToken: generateAccessJWT(updateToken.UserId, generateTimeForAccessToken()),
//             refreshToken: updateToken.token
//         })
//     }catch(error){
//         return next(createError('db error', 500))
//     }

// })


// module.exports = router;