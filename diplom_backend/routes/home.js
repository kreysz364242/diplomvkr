const express = require('express');
const checkJwt = require('../lib/decodeTokens')
const passport = require('passport')
const router = express.Router()
const checkUser = require('../middleware/checkUser')


router.post('/', checkJwt, checkUser, (user, req, resp, next) => {
   
    console.log(user)
    resp.send('ok')
})

module.exports = router