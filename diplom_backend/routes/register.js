const Users = require('../models/UserModel')
const createError = require('../lib/createError')

module.exports = {
    registerNewUser: async(req, resp, next) => {
        const { body: { user } } = req
        const {email, password} = user

        if (!email || !password) 
            return next(createError('Password or email is required', 401))
        try{
            const userDB = await Users.findOne({ where: { email } })
            if(userDB) return next(createError('User alredy exist', 401))
            const newUser = Users.build({ email })
            newUser.setPassword(password)
            await newUser.save()
            resp.status(200).json({message: "register success"})
        }catch(error){
            return next(createError('db error', 500))
        }
    }
}