const Users = require('../models/UserModel')
const express = require('express');
const router = express.Router();

const {generateAccessJWT, generateRefreshJWT} = require('../lib/generateToken')
const {generateTimeForAccessToken, generateTimeForRefreshToken} = require('../lib/generateTimeToToken')
const createError = require('../lib/createError')

router.post('/', async (req, resp, next) => {
    const { body: { user } } = req;
    const {email, password} = user
    console.log(user)
    if (!email) return next(createError('email is required', 401)) 
    if (!password) return next(createError('password is required', 401)) 
    try{
        const userDB = await Users.findOne({ where: { email } })
        if(!userDB) return next(createError('Wrong password or email', 401))
        if(userDB.validatePassword(password)){
            const refreshToken = await generateRefreshJWT(userDB.id, user.fingerprint, generateTimeForRefreshToken())
            return resp.status(200).json({ user: {
                accessToken: generateAccessJWT(userDB.id, generateTimeForAccessToken()),
                refreshToken: refreshToken
            }})
        }else{
            return next(createError('Wrong passwor or email', 401))
        }
    }catch(error){
        console.log(error)
        return next(createError('db error', 500))
    }
})

module.exports = router;