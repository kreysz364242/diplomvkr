
const express = require('express');
const router = express.Router();
const register = require('./register').registerNewUser


router.use('/', require('./home'))
router.use('/api', require('./api'))
router.use(require('../middleware/errors/notFoundError'))
router.use(require('../middleware/errors/allErrors'))

module.exports = router;