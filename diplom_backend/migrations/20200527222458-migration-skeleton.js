'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.removeColumn('Users', 'email', {
        transaction
      });
      await queryInterface.removeColumn('Users', 'salt', {
        transaction
      });
      await queryInterface.removeColumn('Users', 'hash', {
        transaction
      });
     
      await queryInterface.addColumn(
        'Users',
        'nickName', {
          type: Sequelize.STRING,
          allowNull: false
        }, {
          transaction
        }
      );
      await queryInterface.addColumn(
        'Users',
        'auth0Id', {
          type: Sequelize.STRING,
          allowNull: false
        }, {
          transaction
        }
      );
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.addColumn(
        'Users',
        'email', {
          type: Sequelize.STRING,
          allowNull: false
        }, {
          transaction
        }
      );
      await queryInterface.addColumn(
        'Users',
        'salt', {
          type: Sequelize.STRING,
          allowNull: false
        }, {
          transaction
        }
      );
      await queryInterface.addColumn(
        'Users',
        'hash', {
          type: Sequelize.STRING(512),
          allowNull: false
        }, {
          transaction
        }
      );
      await queryInterface.removeColumn('Users', 'nickName', {
        transaction
      });
      await queryInterface.removeColumn('Users', 'auth0Id', {
        transaction
      });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  }
};