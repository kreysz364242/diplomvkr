const { getPageContent } = require('./puppeteer')
const cherio = require('cherio')

async function getImage(url) {
    try {
        const images = []
        const pageContent = await getPageContent(url)
        const $ = cherio.load(pageContent)
        for (const image of Object.values($('.scrolling-gallery__photo'))) {
            const imagePath = $(image).attr('href')
            imagePath && images.push(imagePath)
        }
        if(images.length > 0){
            return images
        } else {
            return null
        }
    }catch{
        return null
    }
    
}

module.exports = getImage