module.exports = {

    arrayFromLength: (number) => {
        return Array.from(new Array(number).keys()).map(k => k+1)
    },

    getCity: (location) =>{
        let splitLocation = location.split(',')
        if(splitLocation[0] && splitLocation[0] !== "") return splitLocation[0]
        else throw new Error('City is missing')
    }
} 

