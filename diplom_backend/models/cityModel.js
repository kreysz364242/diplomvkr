const { Sequelize, sequelize } = require('../lib/sequlize')
const Model = Sequelize.Model
class Citys extends Model {}
Citys.init({
    cityName: {
        type: Sequelize.STRING,
        allowNull: false,
    }
}, {
    timestamps: false,
    sequelize,
    modelName: 'City'
});

module.exports = Citys