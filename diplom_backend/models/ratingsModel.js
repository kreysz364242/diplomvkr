const { Sequelize, sequelize } = require('../lib/sequlize')
const Model = Sequelize.Model
const Users = require('./UserModel')
const Places = require('./PlaceModel')
class Ratings extends Model {}
Ratings.init({
    rating: {
        type: Sequelize.DOUBLE,
        allowNull: false
    }
}, {
    timestamps: false,
    sequelize,
    modelName: 'Rating'
});
Users.hasMany(Ratings)
Places.hasMany(Ratings)
module.exports = Ratings