const { Sequelize, sequelize } = require('../lib/sequlize')
const Places = require('./PlaceModel')
const Model = Sequelize.Model
class Images extends Model {}
Images.init({
    image: {
        type: Sequelize.STRING(1024),
        allowNull: false,
    }
}, {
    timestamps: false,
    sequelize,
    modelName: 'Image'
});
Places.hasMany(Images)
module.exports = Images