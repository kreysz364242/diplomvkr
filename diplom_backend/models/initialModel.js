const Users = require('./UserModel')
const Places = require('./PlaceModel')
const RefreshTokens = require('./refreshTokenModel')
const { Sequelize, sequelize } = require('../lib/sequlize')
const Comments = require('./commentsModel')
const Rating = require('./ratingsModel')
const Citys = require('./cityModel')
const Images = require('./imagesModel')


module.exports = async function () {
    try {
        await Citys.sync({ force: true })
        await Places.sync({ force: true })
        await Images.sync({ force: true })
        await Users.sync({ force: true })
        await RefreshTokens.sync({ force: true })
        await Comments.sync({ force: true })
        await Rating.sync({ force: true })
    }catch(error){
        console.log(error)
    }
    
}
