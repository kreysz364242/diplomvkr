const mongoose = require('../lib/mongoose')
const Schema = mongoose.Schema

let placeSchema = new Schema({

    city: {
        type: String, 
        required: true
    },

    images: {
        type: [String], 
        required: true
    },

    locationName: {
        type: String,
        require: true
    },

    locationDescription: {
        type:String, 
        require: true
    },

    location: {
        type:String, 
        require: true
    }
})



exports.Place = mongoose.model('Place', placeSchema)