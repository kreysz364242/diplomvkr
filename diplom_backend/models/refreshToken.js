const mongoose = require('../lib/mongoose')
const Schema = mongoose.Schema

const RefreshTokenSchema = new Schema({

    user_id: {
        type: String, 
        required: true
    },

    token: {
        type: String, 
        required: false
    },

    fingerprint : {
        type: String,
        required: true
    },

    updateAt : {
        type: Number, 
        required: true
    }
})

exports.RefreshToken = mongoose.model('RefreshToken', RefreshTokenSchema)