const { Sequelize, sequelize } = require('../lib/sequlize')
const Citys = require('./cityModel')
const Model = Sequelize.Model
class Places extends Model {}
Places.init({
    locationName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    locationDescription: {
        type: Sequelize.STRING(1024),
        allowNull: false
    }, 
    location: {
        type: Sequelize.STRING,
        allowNull: false
    }, 
    rating: {
        type: Sequelize.DOUBLE,
        allowNull: true
    },
    rewiewCount: {
        type: Sequelize.INTEGER,
        allowNull: true
    }

}, {
    timestamps: false,
    sequelize,
    modelName: 'Place'
});
Citys.hasMany(Places)
module.exports = Places