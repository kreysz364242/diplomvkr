const mongoose = require('../lib/mongoose')
const Schema = mongoose.Schema
const crypto = require('crypto');



let UsersSchema = new Schema({


    email: {
        type: String, 
        required: true
    },

    hash: {
        type: String,
        require: true
    },

    salt: {
        type:String, 
        require: true
    }
})


UsersSchema.methods.setPassword = function(password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  };

  UsersSchema.methods.validatePassword = function(password) {
    const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    return this.hash === hash;
  }


exports.Users = mongoose.model('Users', UsersSchema)