const Users = require('../models/UserModel')
const passport = require('passport')
const passportJWT = require('passport-jwt')

const ExtractJwt = passportJWT.ExtractJwt
const JwtStrategy = passportJWT.Strategy

passport.use(new JwtStrategy({
    jwtFromRequest : ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey : 'secret'
}, (jwt_payload, next) => {
    console.log(jwt_payload.id)
    Users.findOne( { where: { id : jwt_payload.id } } )
      .then((user) => {
        if(user) {
          return next(null, user)
        }else{
          return next(null, false)
        }
      }).catch(() => {
        next(null, false)
      })
  }));

  module.exports = passport