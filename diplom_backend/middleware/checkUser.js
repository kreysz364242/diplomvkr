const Users = require('../models/UserModel')
require('dotenv').config();

module.exports = async (req, res, next) => {  
    try{
        const { user } = req
        const { sub } = user

        const nickname = user[process.env.AUTH0 + 'nickname']
        const picture = user[process.env.AUTH0 + 'picture'] 
        const roles = user[process.env.AUTH0 + 'roles']
        
        const userDB = await Users.findOne({
            where: {
                auth0Id: sub
            }
        })

        if(!userDB) await Users.create({ auth0Id: sub, nickName: nickname, picture: picture})
        
        return next({nickname, sub, roles})

    } catch(err){
        console.log(err)
        return next(err)
    }
}