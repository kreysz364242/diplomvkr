const express = require('express')
const app = express()
const cors = require('cors')
const Sequelize = require("./lib/sequlize")
const bodyParser = require('body-parser')
const passport = require('./config/passport')
const router = require('./routes/index')

// require('./models/initialModel')()

app.use(bodyParser.urlencoded({
    extended: false
}))
app.use(passport.initialize())
app.use(bodyParser.json())
app.use(cors({
    origin: 'http://localhost:3000'
}))
app.use(router)

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
    console.log('We are live on ' + PORT);
});