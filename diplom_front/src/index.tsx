import React from 'react'
import ReactDOD from 'react-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App'
import { Auth0Provider } from './components/Auth0/react-auth0-spa'
import history from './lib/history';
import config from './auth_config.json';

const onRedirectCallback = (appState?: any) => {
  history.push(
    appState && appState.targetUrl
      ? appState.targetUrl
      : window.location.pathname
  );
};

ReactDOD.render(
  <Auth0Provider
      domain={config.domain}
      client_id={config.clientId}
      redirect_uri={window.location.origin}
      onRedirectCallback={onRedirectCallback}
    >
      <App />
    </Auth0Provider>,
    document.getElementById('root')
)

