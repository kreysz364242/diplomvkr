import { all } from 'redux-saga/effects'

import{
    watchRedirect,
    watchVerifyAccessToken,
    watchLogout,
} from './saga/authSaga'

import{
    watchAddComment
} from './saga/commentsSaga'



export default function* saga(){
    yield all(
        [
            watchRedirect(),
            watchVerifyAccessToken(),
            watchLogout(),
            watchAddComment()
        ]
    )
}