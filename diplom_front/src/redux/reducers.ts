import { combineReducers } from "redux";
import { connectRouter } from 'connected-react-router'
import auth from './reducers/authReducer'
import map from "./reducers/mapReducer";


const createRootReducer = (history : any) => combineReducers({
    router: connectRouter(history),
    auth,
    map
  })
  export default createRootReducer