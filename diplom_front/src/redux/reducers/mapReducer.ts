import IAction from "../../models/IAction";

import IMapState from "../../models/map/IMapState";
import initialMapState from "../../models/map/initialMapState";
import {
    SET_CITY_TO_VIEW, SET_VIEW_CITY
} from '../actions/mapActions'

export default function map(
    state = initialMapState,
    { type, payload }: IAction

): IMapState {

    switch (type) {
        case SET_CITY_TO_VIEW: {
            return { ...state, curPlace: payload }
        }

        case SET_VIEW_CITY: {
            return { ...state, viewCity: payload }
        }
    }
    return state
}