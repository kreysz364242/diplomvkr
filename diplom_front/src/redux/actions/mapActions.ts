import  IAction  from '../../models/IAction'


export const SET_CITY_TO_VIEW = "SET_CITY_TO_VIEW"
export const GET_CITY = "GET_CITY"
export const SET_VIEW_CITY = "SET_VIEW_CITY"


export const setSity = (place: any): IAction =>({
    payload: {...place},
    type: SET_CITY_TO_VIEW
})

export const getCity = (coordinates: any = null, city: string = ''): IAction =>({
    payload: {
        coordinates,
        city
    },
    type: SET_CITY_TO_VIEW
})


export const setViewCity = (city: string) =>({
    payload: city, 
    type: SET_VIEW_CITY
})