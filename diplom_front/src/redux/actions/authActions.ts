import  IAction  from '../../models/IAction'

export const LOGGIN_IN = "LOGGIN_IN"
export const SET_AUTHINTICATE = "SET_AUTHINTICATE"
export const VERIFY_ACCESS_TOKEN = "VERIFY_ACCESS_TOKEN"
export const LOG_OUT = "LOG_OUT"
export const CHECK_AUTH_PROCESS = "CHECK_AUTH_PROCESS"
export const CHECK_AUTH = "CHECK_AUTH"

export const logOut = () : IAction => ({
    type: LOG_OUT
})

export const checkAuthProcess = (isCheckAuth: boolean) : IAction => ({
    payload: isCheckAuth,
    type: CHECK_AUTH_PROCESS
})

export const login = (email : string, password : string) : IAction => ({
    payload : {email, password},
    type: LOGGIN_IN
})

export const checkAuth = (): IAction =>({
    type: CHECK_AUTH
})

export const setAuthinticate = (isAuth : boolean) : IAction => ({
    payload: isAuth,
    type: SET_AUTHINTICATE
})

export const verifyAccessToken = () : IAction =>({
    type: VERIFY_ACCESS_TOKEN
})