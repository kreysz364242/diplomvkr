import  IAction  from '../../models/IAction'

export const ADD_COMMENT = "ADD_COMMENT"
export const ADD_COMMENT_TO_STORE="ADD_COMMENT_TO_STORE"

export const addComment = (comment: any): IAction =>({
    type: ADD_COMMENT,
    payload: comment
})

export const addCommentToStore = (comment: any): IAction =>({
    type: ADD_COMMENT_TO_STORE,
    payload: comment
})