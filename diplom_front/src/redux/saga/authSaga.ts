import { all, call, put, takeEvery } from 'redux-saga/effects'
import IAction from '../../models/IAction'
import {accessWithToken} from '../../clients/accessWithToken'
import {generateDataForToken} from '../../lib/generateCurDataForToken'
import {refreshToken} from '../../clients/refreshToken'
import { createFingerPrint } from '../../lib/createFingerprint'

import{
    LOG_OUT,
    SET_AUTHINTICATE,
    VERIFY_ACCESS_TOKEN,
    CHECK_AUTH,
    CHECK_AUTH_PROCESS
} from '../actions/authActions'
import { LOCATION_CHANGE, push } from 'connected-react-router'
import IUserInfo from '../../models/auth/IUserInfo'





export function* ckeckAuth(action: IAction){
    if(!localStorage.getItem('userInfo')){
        yield put({type: SET_AUTHINTICATE, payload: false})
        yield put({type: CHECK_AUTH_PROCESS, payload: false})
    }else{
        const userData: IUserInfo = JSON.parse(localStorage.getItem('userInfo') || '')
        if(userData.exp < generateDataForToken()){
            try{
                const fingerprint = yield call(() => createFingerPrint())
                yield call(() => refreshToken(JSON.parse(localStorage.getItem('refreshToken') || ''), fingerprint))
                yield put({type: VERIFY_ACCESS_TOKEN})
            }catch{
                yield put({type: SET_AUTHINTICATE, payload: false})
                yield put({type: CHECK_AUTH_PROCESS, payload: false})
            }
            
        }else{
            yield put({type: VERIFY_ACCESS_TOKEN})
        }
            
    }
}

export function* verifyAccessToken(){
    const userData: IUserInfo = JSON.parse(localStorage.getItem('userInfo') || '')
    try{
        yield call(() => accessWithToken(userData.accessToken))
        yield put({type: SET_AUTHINTICATE, payload: true})
        yield put({type: CHECK_AUTH_PROCESS, payload: false})
    }
    catch{
        yield put({type: SET_AUTHINTICATE, payload: false})
        yield put({type: CHECK_AUTH_PROCESS, payload: false})
    }
}

export function* logout(){
    localStorage.clear()
    yield put({type: SET_AUTHINTICATE, payload: false})
    yield put(push('/login'))
}

export function* watchVerifyAccessToken(){
    yield takeEvery(VERIFY_ACCESS_TOKEN, verifyAccessToken)
}

export function* watchRedirect(){
    yield takeEvery(CHECK_AUTH, ckeckAuth)
}

export function* watchLogout(){
    yield takeEvery(LOG_OUT, logout)
}