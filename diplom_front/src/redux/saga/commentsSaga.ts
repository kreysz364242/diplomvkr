import { all, call, put, takeEvery } from 'redux-saga/effects'
import IAction from '../../models/IAction'
import IUserInfo from '../../models/auth/IUserInfo'

import {
    ADD_COMMENT,
    ADD_COMMENT_TO_STORE
} from '../actions/placesViewActions'
import { postDataToApi } from '../../clients/requestToAPI'

export function* addComment(action: IAction) {
    try {
        const { commentText, id, rating, token } = action.payload
        const comment = { commentText, id, rating }
        yield call(() => postDataToApi('api/places/addComment', token, comment))
        yield put({ type: ADD_COMMENT_TO_STORE, action: comment })
    } catch (err) {
        console.log(err)
    }
}

export function* watchAddComment() {
    yield takeEvery(ADD_COMMENT, addComment)
}