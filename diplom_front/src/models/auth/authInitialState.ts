import IAuthState from './IAuthState'

export default {
    email: '',
    password: '',
    isAuthenticated : false,
    isCheckAuth: true
} as IAuthState