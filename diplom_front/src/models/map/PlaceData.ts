export default interface IPlaceData {
  CityId: number
  Comments: Array<any>
  Images: Array<string>
  id: number
  location: string
  locationDescription: string
  locationName: string
  placeRating: number
  rating: number
  rewiewCount: number
}