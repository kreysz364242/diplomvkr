export interface IWithLocation{
    latitude?: number
    longitude?: number
    city?: string
}