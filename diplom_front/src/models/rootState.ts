import IAuthState from './auth/IAuthState'
import IMapState from './map/IMapState';

export default interface IRootState{
    auth: IAuthState
    map: IMapState
}