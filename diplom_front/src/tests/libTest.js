const assert = require('chai').assert
const {
  checkPasswordAndSetFeedBack,
  checkEmailAndSetFeedBack
} = require('../testedFiles/checkValid')

describe('lib', () => {
  it('should return {isValid : true, feedBackText : "Success!"}', () => {
    let res = checkPasswordAndSetFeedBack('2131')
    assert.equal(res.isValid, true)
  })

  it('should return false', () => {
    let res = checkPasswordAndSetFeedBack('2131#@$@!$!')
    assert.equal(res.isValid, false)
  })

  it('Test email valid. Should return true', () => {
    let res = checkEmailAndSetFeedBack('kreysz3642@gmail.com')
    assert.equal(res.isValid, true)
  })

  it('Test email valid. Should return false', () => {
    let res = checkEmailAndSetFeedBack('kreysz3642mail.com')
    assert.equal(res.isValid, false)
  })
})