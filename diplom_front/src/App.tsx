import * as React from 'react'
import { Provider } from 'react-redux'
import { Route, } from 'react-router'
import { ConnectedRouter, routerActions } from 'connected-react-router'
import AuthContainer from './components/auth/AuthContainer';
import RegisterContainer from './components/register/registerContainer'
import { store, history } from './redux/store'
import IRootState from './models/rootState'
import { connectedRouterRedirect } from 'redux-auth-wrapper/history4/redirect'
import HomeContainer from './components/home/home'
import './styles/global.scss'
import { PrivateRoute } from './components/Auth0/PrivateRoute'
import AdminPage from './components/AdminPage/AdminPage';


// const userIsAuthenticated = connectedRouterRedirect({
//   authenticatedSelector: (state: IRootState) => state.auth.isAuthenticated,
//   redirectPath: `/login`,
//   wrapperDisplayName: 'UserIsAuthenticated'
// }) as any

// const userIsNotAuthenticated = connectedRouterRedirect({
//   authenticatedSelector: (state: IRootState) => !state.auth.isAuthenticated,
//   redirectPath: '/',
//   wrapperDisplayName: 'UserIsAuthenticated'
// }) as any

class App extends React.Component {
  render() {

    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <PrivateRoute exact path="/Admin" role={'admin'} component={AdminPage} />
          <Route exact path="/" component={HomeContainer} />
          <Route exact path="/login" component={AuthContainer} />
          <Route exact path="/register" component={RegisterContainer} />
        </ConnectedRouter>
      </Provider>)
  }
}

export default App

