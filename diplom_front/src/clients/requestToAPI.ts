import axios from 'axios'

export const getDataFromApi = (route: string, token: string = "") => {
    return axios.get(`${process.env.REACT_APP_API_ADRESS}${route}`, { headers: { Authorization: `Bearer ${token}` } })
}

export const postDataToApi = (route: string, token: string = "", data: any) => {
    return axios.post(`${process.env.REACT_APP_API_ADRESS}${route}`, { data }, { headers: { Authorization: `Bearer ${token}` } })
}

export const patchDataToApi = (route: string, token: string = "", data: any) => {
    return axios.patch(`${process.env.REACT_APP_API_ADRESS}${route}`, { data }, { headers: { Authorization: `Bearer ${token}` } })
}

export const deleteDataFromApi = (route: string, token: string = "", data: any) => {
    return axios.delete(
        `${process.env.REACT_APP_API_ADRESS}${route}`,
        {
            headers:
            {
                Authorization: `Bearer ${token}`
            }
        })
}

export const getDataFromGoogleApi = (apiAdress: string, params: string) => {
    return axios.get(`${apiAdress}${params}`)
}