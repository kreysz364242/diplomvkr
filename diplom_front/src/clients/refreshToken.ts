import axios from 'axios'
import { createFingerPrint } from '../lib/createFingerprint'
import jwtDecode from 'jwt-decode';
import IUserInfo from '../models/auth/IUserInfo'

export const refreshToken = (refreshToken : string, fingerprint : string) =>{
    return axios.post('http://localhost:8000/api/refreshToken', {fingerprint : fingerprint}, { headers: { Authorization: `Token ${refreshToken}` }})
    .then(
        result =>  {
            let userInfo: IUserInfo = {
                ...jwtDecode(result.data.accessToken),
                accessToken: result.data.accessToken
            }
            localStorage.setItem('userInfo', JSON.stringify(userInfo))
            localStorage.setItem('refreshToken', JSON.stringify(result.data.refreshToken))
    })
    .catch(error => {
        console.log(error.response.data)
        throw new Error(error.response.data)
    })
}