import React from 'react'
import { useAuth0 } from './react-auth0-spa'

const CheckLoadProvider = ({ children }) => {
    const { loading } = useAuth0();

    return (
        loading
            ? <div>Loading...</div>
            : children
    )
}

export default CheckLoadProvider