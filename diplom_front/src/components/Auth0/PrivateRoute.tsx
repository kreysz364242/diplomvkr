import React, { useContext, useEffect, useState } from 'react'
import { Route, Redirect } from 'react-router'
import { useAuth0 } from './react-auth0-spa';

export const PrivateRoute: React.FunctionComponent<any> = ({ component, ...rest }) => {

  const RouteComponent = component
  const { isAuthenticated, user } = useAuth0()
  const [access, setAccess] = useState(false)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    if (isAuthenticated && user) {
      const roles: Array<string> = user[`${process.env.REACT_APP_AUTH0_METHA}role`]
      if (roles && roles.indexOf(rest.role) !== -1) {
        setAccess(true)
      } else {
      }
    }
    setLoading(true)
  }, [])

  return (
    loading
      ?
      access ? <Route {...rest} render={routeProps => <RouteComponent {...routeProps} />} />
        : <Redirect to='/' />
      : <div></div>
  )
}