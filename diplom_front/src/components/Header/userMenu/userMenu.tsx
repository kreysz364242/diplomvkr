import React, { useState } from 'react'
import { Dropdown, DropdownToggle, DropdownItem, DropdownMenu } from 'reactstrap'
import { useAuth0 } from '../../Auth0/react-auth0-spa'
import './userMenu.scss'

const UserMenu = () => {
    const [isDropDownOpen, changeDropdownOpen] = useState(false)
    const { user, logout } = useAuth0()

    return (
        <Dropdown isOpen={isDropDownOpen} toggle={() => changeDropdownOpen(!isDropDownOpen)}>
            <DropdownToggle caret className='userButton'>
                <img src={user.picture} />
            </DropdownToggle>
            <DropdownMenu right>
                <DropdownItem header>{user.nickname}</DropdownItem>
                <DropdownItem divider />
                <DropdownItem onClick={() => logout()}>Logout</DropdownItem>
            </DropdownMenu>
        </Dropdown>
    )
}

export default React.memo(UserMenu)