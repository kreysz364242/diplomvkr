import React, { useContext, useEffect, useState } from 'react'
import Button from '../Button'
import { useAuth0 } from '../Auth0/react-auth0-spa'
import './index.scss'
import UserMenu from './userMenu/userMenu'
import { history } from '../../redux/store'



const Header = () => {
    const { isAuthenticated, loginWithRedirect, getTokenSilently, user } = useAuth0();
    const [isAdmin, setIsAdmin] = useState(false)

    useEffect(() => {
        if (isAuthenticated && user) {
            const roles: Array<string> = user[`${process.env.REACT_APP_AUTH0_METHA}role`]
            if (roles && roles.indexOf('admin') !== -1) {
                setIsAdmin(true)
            } else {
                setIsAdmin(false)
            }
        }

    }, [isAuthenticated, user])

    const switchAdminBuuton = () => {
        if (history.location.pathname === '/Admin')
            return <Button onClick={() => { history.push('/') }}>Map</Button>
        else return <Button onClick={() => { history.push('/Admin') }}>Admin page</Button>
    }

    return (
        <div className='header'>
            <div className='header-content'>
                {isAdmin && switchAdminBuuton()}
                {!isAuthenticated && <Button onClick={() => { loginWithRedirect({}) }}>Login</Button>}
                {isAuthenticated && <UserMenu />}
            </div>
        </div>
    )
}

export default Header