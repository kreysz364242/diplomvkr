import React from 'react'
import './dividingLine.scss'

const DividingLine = () => {
    return(
        <div className={'dividing-line'}></div>
    )
}

export default DividingLine