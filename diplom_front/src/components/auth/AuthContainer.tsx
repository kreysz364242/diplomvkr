import Auth from './Auth'
import { login } from '../../redux/actions/authActions'
import { connect } from 'react-redux'
import IRootState from '../../models/rootState'
import { compose } from 'redux'
import { WithAuth } from '../HOC/withAuth'
import { IWithAuthProps } from '../../models/HOC/IWithAuth'

const mapDispatchToProps = (dispatch : any) =>{
    return{
        dispatch,
        login: (email : string, password: string)  => dispatch(login(email, password)),
    }
}

export default compose(
    WithAuth({isNeedToAuth: false} as IWithAuthProps),
    connect(mapDispatchToProps)
)(Auth)