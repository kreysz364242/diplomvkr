import React, { useState, useEffect } from 'react'
import Map from '../map/map'
import '../../styles/mainPage/mainPage.scss'
import IRootState from '../../models/rootState'
import { connect, useDispatch } from 'react-redux'
import Place from '../placeView/placeView'
import CitySelect from '../CitySelect/CitySelect'
import { Row, Col } from 'reactstrap'
import Search from 'mdi-react/MagnifyIcon'
import Button from '../Button'
import { getDataFromApi } from '../../clients/requestToAPI'
import { setViewCity } from '../../redux/actions/mapActions'
import Header from '../Header'
import Footer from '../Footer'

interface IHomeProps {
    isAuth: boolean
    place: any
    logOut(): void
}


const MainPage = (props: IHomeProps) => {

    const [cities, setCities] = useState(null as any)
    const [choosenCity, setChoosenCity] = useState('')
    const dispatch = useDispatch()

    useEffect(() => {
        const getСities = async () => {
            const { data } = await getDataFromApi('api/places/getCities')
            const cities = data.map((city: any) => city.cityName)

            setCities(cities)
        }
        getСities()
    }, [])

    const handleOnClickSearch = () => {
        if (choosenCity && choosenCity.length !== 0) dispatch(setViewCity(choosenCity))
    }

    const handleSelectChange = (value: string): void => {
        setChoosenCity(value)
    }

    return (
        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between', height: '100vh' }}>
            <Header />
            {cities ?

                <div className="content-container main-page">
                    <div style={{ display: 'grid', gridTemplateColumns: '8fr 4fr', gap: '2em' }}>
                        <div className="">
                            <div className='choose-city-block'>
                                <div style={{ display: 'flex', height: '40px', marginTop: '5px' }}>
                                    <div className='choose-city-block__search-block'>
                                        <Search className='choose-city-block__search-icon' />
                                    </div>
                                    <CitySelect cities={cities} selectOnChange={handleSelectChange} />
                                </div>
                                <Button style={{ marginTop: '5px' }} onClick={handleOnClickSearch}>Search</Button>
                            </div>


                            <div className="main-page-map">
                                <Map />
                            </div>
                        </div>
                        <div className="">
                            {props.place && <Place />}
                        </div>
                    </div>
                </div>
                : <p>Loading...</p>
            }
            <Footer />
        </div>

    )
}

const mapStateToProps = (state: IRootState) => ({
    place: state.map.curPlace
})

export default connect(mapStateToProps)(MainPage)