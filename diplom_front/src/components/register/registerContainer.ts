import { connect } from 'react-redux'
import IRootState from '../../models/rootState'
import Register from './register'
import { withRouter } from 'react-router'
import { compose } from 'redux'
import { WithAuth } from '../HOC/withAuth'
import { IWithAuthProps } from '../../models/HOC/IWithAuth'

interface IAuthProps {
    email : string
    password : string
}


const mapDispatchToProps = (dispatch : any) =>{
    return{
        dispatch,
    }
}

export default compose(
    WithAuth({isNeedToAuth: false} as IWithAuthProps),
    connect(mapDispatchToProps)
)(Register)

