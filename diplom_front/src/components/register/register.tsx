import React, { useState } from 'react'
import { Form, FormGroup, Label, Input, FormFeedback, Button, NavLink } from 'reactstrap';
import IInputValidComponent from '../../models/auth/IInput'
import '../../styles/register/register.scss'
import axios from 'axios'
import { checkEmailAndSetFeedBack, checkPasswordAndSetFeedBack } from '../../lib/chekInputAndSetFeedBack'
import { push } from 'connected-react-router'
import { createFingerPrint } from '../../lib/createFingerprint'


interface IRegisterProps {
    dispatch(func: any): any
}

interface IUserRegisterData {
    email: string,
    password: string,
    repeatPassword: string
}





const Register = (props: IRegisterProps) => {
    const [userRegisterData, setUserRegisterData] = useState({ email: '', password: '', repeatPassword: '' })


    const [emailInputValid, setEmailInputValid] = useState({
        isValid: false,
        feedBackText: "Input your email!"
    } as IInputValidComponent)

    const [passwordInputValid, setPasswordInputValid] = useState({
        isValid: false,
        feedBackText: "Password can't be empty!"
    } as IInputValidComponent)

    const [repeatPasswordInputValid, setRepeatPasswordInputValid] = useState({
        isValid: false,
        feedBackText: "Repeat your password!"
    } as IInputValidComponent)






    const setInputEmail = (email: string) => {
        setEmailInputValid(checkEmailAndSetFeedBack(email))
        setUserRegisterData({ ...userRegisterData, email })
    }

    const setInputPassword = (password: string) => {
        setPasswordInputValid(checkPasswordAndSetFeedBack(password))
        setInputRepeatPassword(userRegisterData.repeatPassword, password)
    }

    const setInputRepeatPassword = (repeatPassword: string, password: string) => {


        let newRepeatPasswordInputValue = checkPasswordAndSetFeedBack(repeatPassword)
        if (newRepeatPasswordInputValue.isValid && (password !== repeatPassword))
            newRepeatPasswordInputValue = { isValid: false, feedBackText: "Passwords are different!" }
        setRepeatPasswordInputValid(newRepeatPasswordInputValue)

        setUserRegisterData({ ...userRegisterData, password, repeatPassword })
    }

    const registerNewUser = () => {
        if (emailInputValid.isValid && passwordInputValid.isValid && repeatPasswordInputValid.isValid) {
            axios.post('http://localhost:5000/register', {
                user: {
                    email: userRegisterData.email,
                    password: userRegisterData.password,
                }
            }).then(
                result => {
                    props.dispatch(push('/login'))
                }
                ).catch(error => {
                    console.log(error.response.data)
                })
        }
    }
    
    return (

        <>
            <div className="register">
                <p>Register on historicalPlacesTGN</p>
                <div className="register-input-area">
                    <Form onClick={e => e.preventDefault()}>
                        <FormGroup>
                            <Label for="email">Input your email</Label>
                            <Input
                                value={userRegisterData.email}
                                onChange={(event: any) => setInputEmail(event.target.value)}
                                valid={emailInputValid.isValid}
                                invalid={!emailInputValid.isValid}
                            />
                            <FormFeedback valid={emailInputValid.isValid}>{emailInputValid.feedBackText}</FormFeedback>
                        </FormGroup>
                        <FormGroup>
                            <Label for="password">Input your password</Label>
                            <Input
                                type="password"
                                value={userRegisterData.password}
                                onChange={(event: any) => setInputPassword(event.target.value)}
                                valid={passwordInputValid.isValid}
                                invalid={!passwordInputValid.isValid}
                            />
                            <FormFeedback valid={passwordInputValid.isValid}>{passwordInputValid.feedBackText}</FormFeedback>
                        </FormGroup>
                        <FormGroup>
                            <Label for="repeat password">Repeat your password</Label>
                            <Input
                                type="password"
                                value={userRegisterData.repeatPassword}
                                onChange={(event: any) => setInputRepeatPassword(event.target.value, userRegisterData.password)}
                                valid={repeatPasswordInputValid.isValid}
                                invalid={!repeatPasswordInputValid.isValid} />
                            <FormFeedback vaild={repeatPasswordInputValid.isValid}>{repeatPasswordInputValid.feedBackText}</FormFeedback>
                        </FormGroup>
                        <FormGroup>
                            <Button onClick={() => registerNewUser()} color="success">register</Button>
                            <NavLink href="#" onClick={() => props.dispatch(push('/login'))}>Login on historicalPlacesTGN</NavLink>
                        </FormGroup>

                    </Form>
                </div>
            </div>
        </>
    )


}

export default Register