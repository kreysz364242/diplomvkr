import React from 'react'
import { Button } from 'reactstrap'
import './Button.scss'

interface IButton {
    className?: string
    style?: Object
    children?: any
    onClick?: () => void
}

const MyButton: React.FunctionComponent<IButton> = ({ className: userClassName = '', style: userStyle = {}, children, onClick }) => {
    return (
        <Button color="success" onClick={() => onClick && onClick()} className={`${userClassName} myButton`} style={userStyle}>{children}</Button>
    )
}

export default MyButton