import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import './comments.scss'
import { Button, Comment, Form, Header } from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css'
import Rating from '@material-ui/lab/Rating';
import { addComment } from '../../../redux/actions/placesViewActions';
import Box from '@material-ui/core/Box';
import { useDispatch } from 'react-redux';
import { useAuth0 } from '../../Auth0/react-auth0-spa';
import { postDataToApi, getDataFromApi } from '../../../clients/requestToAPI';




const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    inline: {
        display: 'inline',
    },
}));



const Comments = (props: any) => {
    const { handleSetPlaceRating } = props
    const [commentText, setCommentText] = useState('')
    const [rating, setRating] = useState(0)
    const { getTokenSilently, isAuthenticated } = useAuth0();
    const { user } = useAuth0()

    const addCommentToPlace = () => {
        const addCommentWithToken = async () => {
            try {
                const token = await getTokenSilently()
                const comment = { commentText, id: props.place.id, rating }
                await postDataToApi('api/places/addComment', token, comment)
                const newComments = { ...props.comments, alreadyRewiew: true }
                newComments.commentsVal.push({ commentText, id: props.place.id, rating, nickName: user.nickname, picture: user.picture })
                props.setComments(newComments)

                const resp = await getDataFromApi(`api/places/getRating?PlaceId=${props.place.id}`)
                handleSetPlaceRating(resp.data)

            } catch (err) {
                console.log(`ERROR ${err}`)
            }
        }
        if (commentText !== '') {
            addCommentWithToken()
        }
    }

    const classes = useStyles();
    return (
        <div className={'container comments-container'}>
            <div className="raw rating-container">
                <div className={'rating col-12'}>
                    <p className='rating__heading'>Rewiews</p>
                </div>
            </div>
            <div className={'comments'}>
                <List className={classes.root}>
                    {props.comments.commentsVal.length !== 0 ?
                        props.comments.commentsVal.map((comment: any, id: number) => {
                            return (
                                <ListItem key={id} alignItems="flex-start">
                                    <ListItemAvatar>
                                        <Avatar alt="Remy Sharp" src={comment.picture ? comment.picture : ''} />
                                    </ListItemAvatar>
                                    <ListItemText
                                        primary={comment.nickName}
                                        secondary={
                                            <React.Fragment>
                                                {comment.commentText}
                                                <Box component="fieldset" mb={3} borderColor="transparent">
                                                    <Rating name="read-only" value={comment.rating} readOnly />
                                                </Box>
                                            </React.Fragment>
                                        }
                                    />
                                </ListItem>
                            )
                        })
                        : <div></div>
                    }
                </List>
                {!props.comments.alreadyRewiew && isAuthenticated &&
                    <div className="comments__input" style={{ marginTop: '1em' }}>
                        <Form className='comments__textfield'>
                            <Form.TextArea value={commentText} onChange={(event: any) => { setCommentText(event.target.value) }} rows='3' style={{ minHeight: 50 }} />
                            <Box component="fieldset" mb={3} borderColor="transparent">

                                <Rating
                                    name="simple-controlled"
                                    value={rating}
                                    onChange={(event, newValue) => {
                                        setRating(newValue || 0);
                                    }}
                                />
                            </Box>
                            <Button content='Add Reply' onClick={() => addCommentToPlace()} labelPosition='left' icon='edit' primary />
                        </Form>

                    </div>}
            </div>
        </div>
    )
}

export default Comments