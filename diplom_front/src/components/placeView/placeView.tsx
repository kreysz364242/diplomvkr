import React, { useState, useEffect } from 'react'
import IRootState from '../../models/rootState'
import { connect } from 'react-redux'
import {
    Card, CardImg, CardText, CardBody,
    CardLink, CardHeader,
    Carousel,
    CarouselItem,
    CarouselControl,
    CarouselIndicators,
    CarouselCaption
} from 'reactstrap';
import '../../styles/placeView/placeView.scss'
import Comments from './comments/comments';
import { getDataFromApi } from '../../clients/requestToAPI';
import { useAuth0 } from '../Auth0/react-auth0-spa';
import './placeView.scss'
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';

const Place = (props: any) => {
    const { place } = props
    const [activeIndex, setActiveIndex] = useState(0);
    const [animating, setAnimating] = useState(false);
    const [comments, setComments] = useState({} as any)
    const [showComments, setShowComments] = useState(false)
    const [placeRating, setPlaceRating] = useState(0)

    const { user } = useAuth0();

    useEffect(() => {
        setActiveIndex(0)
        setAnimating(false)
    }, [props])

    useEffect(() => {
        const getPlaceComments = async () => {
            try {
                let resp
                if (user)
                    resp = await getDataFromApi(`api/places/getPlaceComments?PlaceId=${place.id}&nickName=${user.nickname}`)
                else
                    resp = await getDataFromApi(`api/places/getPlaceComments?PlaceId=${place.id}&nickName=`)
                setComments(resp.data)
                setPlaceRating(place.placeRating)
            } catch (err) {
                console.log(err)
            }

        }
        getPlaceComments()
        setShowComments(false)
    }, [props])

    const handleSetPlaceRating = (rating: number) => {
        setPlaceRating(rating)
    }

    const next = () => {
        if (animating) return;
        const nextIndex = activeIndex === place.Images.length - 1 ? 0 : activeIndex + 1;
        setActiveIndex(nextIndex);
    }

    const previous = () => {
        if (animating) return;
        const nextIndex = activeIndex === 0 ? place.Images.length - 1 : activeIndex - 1;
        setActiveIndex(nextIndex);
    }

    const goToIndex = (newIndex: any) => {
        if (animating) return;
        setActiveIndex(newIndex);
    }

    const slides = place.Images.map((item: any, index: number) => {
        return (
            <CarouselItem
                onExiting={() => setAnimating(true)}
                onExited={() => setAnimating(false)}
                key={index}
            >
                <img src={item} />
            </CarouselItem>
        );
    });

    return (

        showComments
            ? <Card className="card card-comments">
                <CardHeader className="card__header">{`${place.locationName}`}</CardHeader>

                <CardBody>
                    <Comments {...{ place, comments, setComments, handleSetPlaceRating }} />
                    <CardLink onClick={() => setShowComments(!showComments)} href="#">{`Return to description`}</CardLink>
                </CardBody>
            </Card>

            : <Card className="card">
                <CardHeader className="card__header">{place.locationName}</CardHeader>
                <div>
                    {place.Images.length === 1
                        ?
                        <div className="container-image">
                            <CardImg top className="image-block" src={place.Images[0]} alt="Card image cap" />
                        </div>
                        :
                        <div className="container-carousel">
                            <Carousel className="carousel"
                                activeIndex={activeIndex}
                                next={next}
                                previous={previous}
                            >
                                <CarouselIndicators items={place.Images} activeIndex={activeIndex} onClickHandler={goToIndex} />
                                {slides}
                                <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
                                <CarouselControl direction="next" directionText="Next" onClickHandler={next} />
                            </Carousel>
                        </div>}

                </div>
                <CardBody>
                    <CardText>{place.locationDescription}</CardText>
                    <div className='ratin-with-comments-link'>
                        <CardLink onClick={() => setShowComments(!showComments)} href="#">{`Comments (${comments.commentsVal ? comments.commentsVal.length : 0})`}</CardLink>
                        {
                            placeRating &&
                            <Box component="fieldset" mb={3} borderColor="transparent">
                                <Rating name="read-only" value={placeRating} readOnly />
                            </Box>
                        }
                    </div>
                </CardBody>
            </Card>
    )
}

const mapStateToProps = (state: IRootState) => ({
    place: state.map.curPlace
})

export default connect(mapStateToProps)(Place)