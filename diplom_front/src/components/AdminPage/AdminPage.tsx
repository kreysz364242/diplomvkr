import React, { useEffect, useState } from 'react'
import Header from '../Header'
import Footer from '../Footer'
import { Row, Col, FormGroup, Label, Input, Container, Table, Button } from 'reactstrap'
import { getDataFromApi, deleteDataFromApi } from '../../clients/requestToAPI'
import './AdminPage.scss'
import { useAuth0 } from '../Auth0/react-auth0-spa'
import AdminModal from './AdminModal'
import MyButton from '../Button'
import IPlaceData from '../../models/map/PlaceData'
import ScrapperForm from './ScrapperForm/ScrapperForm'

const AdminPage = React.memo(() => {
    const [cities, setCities] = useState([] as Array<string>)
    const [selectCity, setSelectCity] = useState('' as string)
    const [places, setPlaces] = useState(undefined as any)
    const { getTokenSilently } = useAuth0()
    const [modal, toggleModal] = useState(false)
    const [changeModal, setChangeModal] = useState(false)
    const [curPlaceData, setCurPlaceData] = useState({} as IPlaceData)

    useEffect(() => {
        const getСities = async () => {
            try {
                const { data } = await getDataFromApi('api/places/getCities')
                const cities = data.map((city: any) => city.cityName)
                setCities(cities)
            } catch (error) {
                console.error(error)
            }

        }
        getСities()
    }, [])

    useEffect(() => {
        if (cities && cities.length !== 0)
            setSelectCity(cities[0])
    }, [cities])


    useEffect(() => {
        if (selectCity.length !== 0) {
            getPlaces()
        }
    }, [selectCity])

    const getPlaces = async () => {
        try {
            const { data } = await getDataFromApi(`api/places/getPlaces?cityName=${selectCity}`)
            setPlaces(data)
        }
        catch (error) {
            console.error(error)
        }
    }

    const toggle = (changeModal: boolean = false) => {
        toggleModal(!modal)
        setChangeModal(changeModal)
    }


    const deletePlace = async (placeId: number) => {
        console.log(placeId)
        try {
            const token = await getTokenSilently()
            await deleteDataFromApi(`api/admin/deletePlace?placeId=${placeId}`, token, placeId)
            const newPlaces = places.filter((place: any) => place.id !== placeId)
            setPlaces(newPlaces)
        } catch (error) {
            console.error(error)
        }
    }

    return (
        <div style={{ display: 'flex', flexDirection: 'column', height: '100vh' }}>
            <Header />
            <Container className='admin-page-content'>
                {cities && <FormGroup row>
                    <Label for="exampleSelect" sm={3}>Выберите город</Label>
                    <Col sm={9}>
                        <Input type="select" value={selectCity} name="select" onChange={(event) => setSelectCity(event.target.value)} id="exampleSelect">
                            {cities.map((city: string) => <option>{city}</option>)}
                        </Input>
                    </Col>
                </FormGroup>}
                <MyButton onClick={() => {
                    setCurPlaceData({
                        location: '',
                        locationName: '',
                        locationDescription: ''
                    } as IPlaceData)
                    toggle()
                    console.log(cities)
                }}>Add Place</MyButton>
                <div>
                    <ScrapperForm />
                </div>
                <Table hover>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Location name</th>
                            <th>location</th>
                            <th>Controll</th>
                        </tr>
                    </thead>
                    <tbody style={{ overflowY: 'auto' }}>
                        {places && places.map((place: IPlaceData) =>
                            <tr key={place.id}>
                                <th scope="row">{place.id}</th>
                                <td>{place.locationName}</td>
                                <td>{place.location}</td>
                                <td style={{ display: 'flex' }}>
                                    <Button
                                        color="primary"
                                        onClick={() => {
                                            setCurPlaceData(place)
                                            toggle(true)
                                        }}
                                    >
                                        Change
                                    </Button>
                                    <Button color="danger" onClick={() => deletePlace(place.id)} style={{ marginLeft: '1em' }}>Delete</Button>
                                </td>
                            </tr>)}
                    </tbody>
                </Table>
            </Container>
            <AdminModal
                modal={modal}
                toggle={toggle}
                changeModal={changeModal}
                curCity={selectCity}
                placeData={curPlaceData}
                getPlaces={getPlaces} />
        </div>
    )
})

export default AdminPage