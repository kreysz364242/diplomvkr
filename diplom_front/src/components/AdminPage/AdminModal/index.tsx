import React, { useEffect, useState } from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input } from 'reactstrap';
import IPlaceData from '../../../models/map/PlaceData';
import { useAuth0 } from '../../Auth0/react-auth0-spa';
import { patchDataToApi, postDataToApi } from '../../../clients/requestToAPI';

interface IAdminModal {
    modal: boolean
    changeModal: boolean
    placeData: IPlaceData
    curCity: string
    getPlaces: () => void
    toggle: () => void
}

const AdminModal: React.FunctionComponent<IAdminModal> = ({ modal, toggle, changeModal, placeData, curCity, getPlaces }) => {
    const [modalPlaceData, setModalPlaceData] = useState({} as IPlaceData)
    const { getTokenSilently } = useAuth0()

    const checkInputRequier = (): boolean => {
        const { location, locationName, locationDescription } = modalPlaceData
        if (location.length !== 0 || locationName.length !== 0, locationDescription.length !== 0)
            return true
        return false
    }

    const addPlace = async () => {
        try {
            if (checkInputRequier()) {
                const token = await getTokenSilently()
                await postDataToApi('api/admin/addPlace', token, { ...modalPlaceData, cityName: curCity })
                toggle()
                getPlaces()
            }
        } catch (error) {
            console.log(error)
        }
    }

    const changePlace = async () => {
        try {
            if (checkInputRequier()) {
                const token = await getTokenSilently()
                await patchDataToApi('api/admin/changePlace', token, modalPlaceData)
                toggle()
                getPlaces()
            }
        } catch (error) {
            console.log(error)
        }
    }

    const controlButtonClick = () => {
        if (changeModal)
            changePlace()
        else
            addPlace()
    }

    const changeInputData = (modalDataFiled: string, dataParam: string | number) => {
        setModalPlaceData((data: IPlaceData) => ({
            ...data, [modalDataFiled]: dataParam
        }))
    }
    useEffect(() => {
        setModalPlaceData(placeData)
    }, [placeData])

    return (
        <Modal isOpen={modal} toggle={toggle}>
            <ModalHeader toggle={toggle}>Modal title</ModalHeader>
            <ModalBody>
                <Form>
                    <FormGroup>
                        <Label for="location">Location</Label>
                        <Input
                            onChange={(event: any) => { changeInputData('location', event.target.value) }}
                            value={modalPlaceData.location}
                            type="text"
                            id="location"
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="locationName">Location name</Label>
                        <Input
                            onChange={(event: any) => { changeInputData('locationName', event.target.value) }}
                            value={modalPlaceData.locationName}
                            type="text"
                            id="locationName"
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="locationDescription">Location description</Label>
                        <Input
                            onChange={(event: any) => { changeInputData('locationDescription', event.target.value) }}
                            value={modalPlaceData.locationDescription}
                            type="textarea"
                            id="locationDescription" />
                    </FormGroup>
                </Form>
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={controlButtonClick}>{changeModal ? 'Change' : 'Add Place'}</Button>{' '}
                <Button color="secondary" onClick={toggle}>Cancel</Button>
            </ModalFooter>
        </Modal>
    )
}

export default AdminModal