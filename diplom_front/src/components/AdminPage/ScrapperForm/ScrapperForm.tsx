import React, { useState } from 'react'
import { Form, Label, Input, FormGroup } from 'reactstrap'
import { useAuth0 } from '../../Auth0/react-auth0-spa'
import { patchDataToApi, postDataToApi } from '../../../clients/requestToAPI'
import Button from '../../Button'
import './ScrapperForm.scss'

interface ScrapperData {
  url: string
  numberOfPages: number
  city: string
}

const ScrapperForm = () => {
  const [scrapperData, setScrapperData] = useState({
    url: '',
    numberOfPages: 0,
    city: ''
  } as ScrapperData)
  const { getTokenSilently } = useAuth0()

  const scrapp = async () => {
    try {
      if (checkInputRequier()) {
        const token = await getTokenSilently()
        await postDataToApi('api/admin/useScrapper', token, scrapperData)
      }
    } catch (error) {
      console.log(error)
    }
  }

  const controlButtonClick = () => {
    if (checkInputRequier()) {
      scrapp()
    }
  }

  const checkInputRequier = (): boolean => {
    const { url, numberOfPages, city } = scrapperData
    if (url.length !== 0 || numberOfPages !== 0, city.length !== 0)
      return true
    return false
  }

  const changeInputData = (modalDataFiled: string, dataParam: string | number) => {
    setScrapperData((data: ScrapperData) => ({
      ...data, [modalDataFiled]: dataParam
    }))
  }

  return (
    <div className={`scrapper`} style={{ margin: '2em 0' }}>
      <Form>
        <FormGroup>
          <Label for="url">Url</Label>
          <Input
            onChange={(event: any) => { changeInputData('url', event.target.value) }}
            value={scrapperData.url}
            type="text"
            id="url"
          />
        </FormGroup>
        <FormGroup>
          <Label for="numberOfPages">Number of pages</Label>
          <Input
            onChange={(event: any) => { changeInputData('numberOfPages', event.target.value) }}
            value={scrapperData.numberOfPages}
            type="number"
            id="numberOfPages"
          />
        </FormGroup>
        <FormGroup>
          <Label for="city">City</Label>
          <Input
            onChange={(event: any) => { changeInputData('city', event.target.value) }}
            value={scrapperData.city}
            type="text"
            id="city" />
        </FormGroup>
      </Form>
      <Button
        onClick={() => {
          controlButtonClick()
        }}
      >
        Scrapping
      </Button>
    </div>
  )
}

export default ScrapperForm