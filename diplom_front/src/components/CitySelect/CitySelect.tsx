import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { makeStyles } from '@material-ui/core/styles';
import './CitySelect.scss'

const useStyles = makeStyles({
  option: {
    fontSize: 15,
    '& > span': {
      marginRight: 10,
      fontSize: 18,
    },
  },
});

interface ICitySelect {
  cities: string[]
  selectOnChange: (value: string) => void
}


const CitySelect = ({ cities, selectOnChange }: ICitySelect) => {
  const classes = useStyles();


  return (
    <Autocomplete
      id="country-select-demo"
      options={cities}
      classes={{
        option: classes.option,
      }}
      onChange={(event, value) => selectOnChange(value || '')}
      autoHighlight
      getOptionLabel={(option: any) => option}
      renderOption={(option: any) => (
        <React.Fragment>
          {option}
        </React.Fragment>
      )}
      renderInput={(params: any) => (
        <TextField
          {...params}
          placeholder="Choose a city"
          variant="outlined"
          color='secondary'
          inputProps={{
            ...params.inputProps,
            autoComplete: 'new-password', // disable autocomplete and autofill
          }}
        />
      )}
    />
  );
}


export default CitySelect