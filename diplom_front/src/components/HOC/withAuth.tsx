import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import IRootState from '../../models/rootState'
import {checkAuthProcess, checkAuth} from '../../redux/actions/authActions'
import '../../styles/HOC/preloader.scss'
import { IWithAuthProps } from '../../models/HOC/IWithAuth'
import { push } from 'connected-react-router'

interface ConnectAuthRedirect{
    checkAuthProcess(isCheckAuth: boolean): void
    checkAuth(): void
    push(path: any): void
    isCheckAuth: boolean
    isAuth: boolean
}

const mapStateToProps =  (state : IRootState) =>({
    isAuth: state.auth.isAuthenticated,
    isCheckAuth: state.auth.isCheckAuth
})

const mapDispatchToProps = {
    checkAuthProcess,
    checkAuth,
    push
}




export const WithAuth = (props: IWithAuthProps) => (Component: any) =>{
    let {isNeedToAuth} = props
    
    const ConnectAuthRedirect = (ownProps: ConnectAuthRedirect) =>{
        let {checkAuthProcess, isAuth, isCheckAuth, checkAuth, push } = ownProps


        const checkAccessToPage = (): boolean =>{
            if(isAuth === isNeedToAuth) 
                return true
            else {
                redirect()
                return false
            }
        }

        const redirect = (): void =>{
            isNeedToAuth && push('/login')
            !isNeedToAuth && push('/')
        }

        useEffect(()=>{
            checkAuthProcess(true)
            checkAuth()
        }, [])

        return(
            isCheckAuth 
                ? <div className="preloader">
                   <div className="loader"></div> 
                  </div>
                : checkAccessToPage() ? <Component /> : <></>
        )
    }

    return connect(mapStateToProps, mapDispatchToProps)(ConnectAuthRedirect)
    
}