import React, { useEffect, useState } from 'react'
import { geolocated, GeolocatedProps } from "react-geolocated";
import { IWithLocation } from '../../models/HOC/IWithLocation'
import '../../styles/map/map.scss'
import { useSelector } from 'react-redux';
import IRootState from '../../models/rootState';

const WithLocation = (Component: any) => {
    const ComponentWithLocation = (geolocatedProps: GeolocatedProps): JSX.Element => {
        return (
            <div className="map-user-coord">
                {!geolocatedProps.isGeolocationAvailable && !geolocatedProps.isGeolocationEnabled && !geolocatedProps.coords
                    ? <div>Error geolocation</div>
                    : geolocatedProps.coords && <div>
                        <Component {...{ latitude: geolocatedProps.coords?.latitude, longitude: geolocatedProps.coords?.longitude } as IWithLocation} />
                    </div>
                }

            </div>
        )
    }
    return geolocated({
        positionOptions: {
            enableHighAccuracy: false,
        },
        userDecisionTimeout: 5000,
    })(ComponentWithLocation);
}

export default WithLocation