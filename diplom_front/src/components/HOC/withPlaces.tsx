import React, { useState, useEffect } from 'react'
import { IWithLocation } from '../../models/HOC/IWithLocation'
import { getDataFromApi, getDataFromGoogleApi } from '../../clients/requestToAPI'
import '../../styles/HOC/preloaderMap.scss'
import { useSelector } from 'react-redux'

interface ILoadError {
    isError: boolean,
    errorMessage: string
}


const TakePlaces = (Component: any) => React.memo((props: IWithLocation) => {
    const NewComponent = () => {
        const [isLoading, setIsLoading] = useState(false)
        const [placesData, setPlacesData] = useState([{}])
        const [loadError, setLoadError] = useState({
            isError: false,
            errorMessage: ''
        } as ILoadError)
        const [isError, setIsError] = useState(false)
        const [centerLocation, setCenterLocation] = useState({} as any)

        const viewCity = useSelector(state => (state as any).map.viewCity)

        const getCenterCity = async (cityName: string) => {
            try {
                const data = await getDataFromGoogleApi(
                    "https://maps.googleapis.com/maps/api/geocode/",
                    `json?address=${cityName}&language=ru&key=${process.env.REACT_APP_API_KEY}`
                )

                setCenterLocation((data as any).data.results[0].geometry.location)
                return (data as any).data.results[0].geometry.location
            }
            catch (err) {
                console.error(`ERROR ${err}`)
                setIsError(true)
                setIsLoading(false)
            }
        }

        const getPlaces = async (city: string) => {
            try {
                const places = await getDataFromApi(`api/places/getPlaces?cityName=${city}`)
                let placesMap = []
                if (places)
                    for (let place of places.data) {

                        let data = await getDataFromGoogleApi(
                            "https://maps.googleapis.com/maps/api/geocode/",
                            `json?address=${place.location}&key=${process.env.REACT_APP_API_KEY}`
                        )
                        let geometry
                        try {
                            let geomData = data.data.results[0]
                            geometry = geomData.geometry
                            placesMap.push({
                                place,
                                geometry
                            })
                        } catch (error) {
                            console.error('Error find place')
                        }
                    }

                setPlacesData([...placesMap])
            } catch (err) {
                console.error(`ERROR ${err}`)
                setIsError(true)
                setIsLoading(false)
            }
        }

        const getCityName = async (lat: number, lng: number) => {
            try {
                let data = await getDataFromGoogleApi(
                    "https://maps.googleapis.com/maps/api/geocode/",
                    `json?latlng=${lat},${lng}&result_type=locality&language=ru&key=${process.env.REACT_APP_API_KEY}`
                )
                console.log(data)
                let curCity
                if (data.data.results.length !== 0) {
                    let { address_components } = data.data.results[0]
                    curCity = address_components.find((addressComponetn: any) => addressComponetn.types.find((type: string) => type === "locality")).long_name
                } else {
                    let cityCode: string = data.data.plus_code.compound_code
                    curCity = cityCode.substr(cityCode.indexOf(' ') + 1, (cityCode.indexOf(',') - (cityCode.indexOf(' ') + 1)))
                }

                return curCity

            } catch (err) {
                console.error(`ERROR ${err}`)
                setIsError(true)
                setIsLoading(false)
            }
        }

        useEffect(() => {
            console.log(viewCity)
            setIsError(false)
            setIsLoading(true)
            const loadDataForMap = async () => {
                if (viewCity) {
                    await getCenterCity(viewCity)
                    await getPlaces(viewCity)
                    setIsLoading(false)
                } else if (props.latitude && props.longitude) {
                    const curCity = await getCityName(props.latitude, props.longitude)
                    await getCenterCity(curCity)
                    await getPlaces(curCity)
                    setIsLoading(false)
                } else {
                    setIsError(true)
                    setIsLoading(false)
                }
            }

            loadDataForMap()
        }, [viewCity, props])


        return (
            isLoading ?
                <div className="preloader-map">
                    <div className="loader"></div>
                </div>
                : isError
                    ? <div>{`Error`}</div>
                    : centerLocation &&
                    <Component {...{
                        location: {
                            latitude: centerLocation.lat,
                            longitude: centerLocation.lng
                        }, placesData, setPlacesData
                    }} />
        )
    }

    return NewComponent()
})

export default TakePlaces