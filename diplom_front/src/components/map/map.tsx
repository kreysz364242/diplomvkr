import React, { useEffect } from 'react'
import { GoogleMap, withScriptjs, withGoogleMap, Marker } from 'react-google-maps'
import WithLocation from '../HOC/withLocation'
import withPlaces from '../HOC/withPlaces'
import { compose, withProps, withState, withHandlers } from 'recompose'
import image from '../../styles/img/mapImage.png'
import mapStyle from '../../styles/map/mapDefaultStyle'
import '../../styles/map/map.scss'
import { connect } from 'react-redux'
import { setSity } from '../../redux/actions/mapActions'

declare const window: any
declare const google: any

interface IRef {
    map?: GoogleMap
}

interface IIconSize {
    width: number
    height: number
}

const Map = compose(
    withProps({
        googleMapURL: `https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${process.env.REACT_APP_API_KEY}`,
        loadingElement: <div style={{ height: `100%`, width: '100%' }} />,
        containerElement: <div style={{ height: `100%`, width: '100%' }} />,
        mapElement: <div style={{ height: `100%`, width: '100%' }} />,
    }),

    withState('iconSize', 'setIconSize', { width: 16, height: 16 } as IIconSize),
    withState('zoom', 'onZoomChange', 15),
    withHandlers(() => {
        const refs = {
            map: undefined,
        } as IRef

        return {
            onMapMounted: () => (ref: any) => {
                refs.map = ref
            },
            onZoomChanged: (props: any) => () => {
                const { onZoomChange } = props
                refs.map !== undefined && onZoomChange(refs.map.getZoom())
            },
            getRef: () => () => {
                return refs
            },

        }
    }),
    withScriptjs,
    withGoogleMap
)((props: any) => {
    const { location, placesData, zoom, iconSize, setIconSize, onMarkerClick, onZoomChange, setSity } = props


    useEffect(() => {
        if (zoom < 13) onZoomChange(13)
        else {
            if (zoom < 14)
                setIconSize({ width: 16 * (zoom / 15), height: 16 * (zoom / 15) })
            else setIconSize({ width: 16, height: 16 })
        }

    }, [zoom])
    return (
        <GoogleMap
            defaultCenter={{ lat: location.latitude, lng: location.longitude }}
            zoom={zoom}
            ref={props.onMapMounted}
            onZoomChanged={props.onZoomChanged}
            defaultOptions={{ styles: mapStyle }}
        >
            {placesData.map((place: any, index: number) =>
                (
                    <Marker
                        key={index}
                        onClick={() => setSity(place.place)}
                        position={{ lat: place.geometry.location.lat, lng: place.geometry.location.lng }}
                        icon={{ url: image, scaledSize: new window.google.maps.Size(iconSize.width, iconSize.height) }}
                    >
                    </Marker>
                ))}
            {/* <Marker
                position={{ lat: location.latitude, lng: location.longitude }}
            >

            </Marker> */}
        </GoogleMap>

    )
}
);
const mapDispatchToProps = {
    setSity
}

const ConnectedMap = connect(null, mapDispatchToProps)(Map)


export default compose(
    WithLocation,
    withPlaces
)(ConnectedMap)